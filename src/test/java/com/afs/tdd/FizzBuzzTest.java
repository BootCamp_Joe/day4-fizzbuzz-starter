package com.afs.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FizzBuzzTest {

    @Test
    public void should_say_number_when_sayFizzBuzz_given_input_normal_number() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        Integer number = 1;

        //when
        String actual = fizzBuzz.say(number);

        //then
        assertEquals("1", actual);
    }

    @Test
    public void should_say_fizz_when_sayFizzBuzz_given_input_3() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        Integer number = 3;

        //when
        String actual = fizzBuzz.say(number);

        //then
        assertEquals("Fizz", actual);
    }

    @Test
    public void should_say_fizz_when_sayFizzBuzz_given_input_multiple_of_3() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        Integer number = 9;

        //when
        String actual = fizzBuzz.say(number);

        //then
        assertEquals("Fizz", actual);
    }

    @Test
    public void should_say_buzz_when_sayFizzBuzz_given_input_5() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        Integer number = 5;

        //when
        String actual = fizzBuzz.say(number);

        //then
        assertEquals("Buzz", actual);
    }

    @Test
    public void should_say_fizz_when_sayFizzBuzz_given_input_multiple_of_5() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        Integer number = 10;

        //when
        String actual = fizzBuzz.say(number);

        //then
        assertEquals("Buzz", actual);
    }

    @Test
    public void should_say_fizzBuzz_when_sayFizzBuzz_given_input_15() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        Integer number = 15;

        //when
        String actual = fizzBuzz.say(number);

        //then
        assertEquals("FizzBuzz", actual);
    }

    @Test
    public void should_say_fizzBuzz_when_sayFizzBuzz_given_input_multiple_of_3_and_5() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        Integer number = 30;

        //when
        String actual = fizzBuzz.say(number);

        //then
        assertEquals("FizzBuzz", actual);
    }

    @Test
    public void should_throw_Exception_when_sayFizzBuzz_given_input_negative() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        Integer number = -1;

        //when
        assertThrows(IllegalArgumentException.class, () -> {
            fizzBuzz.say(number);
        });

        //then
    }

    @Test
    public void should_say_whizz_when_sayFizzBuzz_given_input_multiple_of_7() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        Integer number = 7;

        //when
        String actual = fizzBuzz.say(number);

        //then
        assertEquals("Whizz", actual);
    }

    @Test
    public void should_say_fizzWhizz_when_sayFizzBuzz_given_input_multiple_of_3_and_7() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        Integer number = 21;

        //when
        String actual = fizzBuzz.say(number);

        //then
        assertEquals("FizzWhizz", actual);
    }

    @Test
    public void should_say_buzzWhizz_when_sayFizzBuzz_given_input_multiple_of_5_and_7() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        Integer number = 35;

        //when
        String actual = fizzBuzz.say(number);

        //then
        assertEquals("BuzzWhizz", actual);
    }

    @Test
    public void should_say_fizzbuzzWhizz_when_sayFizzBuzz_given_input_multiple_of_3_and_5_and_7() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        Integer number = 105;

        //when
        String actual = fizzBuzz.say(number);

        //then
        assertEquals("FizzBuzzWhizz", actual);
    }




}
