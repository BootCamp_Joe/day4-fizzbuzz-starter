package com.afs.tdd;

public class FizzBuzz {

    public String say(Integer integer) {
        if(integer <= 0) throw new IllegalArgumentException();
        if(integer % 3 == 0 && integer % 5 == 0 && integer % 7 == 0) return "FizzBuzzWhizz";
        if(integer % 3 == 0 && integer % 5 == 0) return "FizzBuzz";
        if(integer % 3 == 0 && integer % 7 == 0) return "FizzWhizz";
        if(integer % 5 == 0 && integer % 7 == 0) return "BuzzWhizz";
        if (integer % 3 == 0) return "Fizz";
        if (integer % 5 == 0) return "Buzz";
        if (integer % 7 == 0) return "Whizz";
        return integer.toString();
    }
}